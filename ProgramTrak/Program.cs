﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Timers;

namespace ProgramTrak
{
    class Program
    {
        static void Main(string[] args)
        {
            // No DB file then make a new one.
            if (!File.Exists("ProgramTrak.sqlite"))
            {
                SQLiteConnection.CreateFile("ProgramTrak.sqlite");
                // Setup table.
                SQLiteConnection sqliteConnection = new SQLiteConnection("Data Source=ProgramTrak.sqlite;Version=3;");
                sqliteConnection.Open();
                SQLiteCommand sqliteCommand = new SQLiteCommand("CREATE TABLE TestTable(ProcessName VARCHAR(100), ActiveMins INT)", sqliteConnection);
                sqliteCommand.ExecuteNonQuery();
                sqliteConnection.Close();
            }

            Timer timer = new Timer();
            timer.Interval = 60000;
            timer.Elapsed += TimerTickEvent;
            timer.AutoReset = true;
            timer.Enabled = true;

            Console.WriteLine("Starting process logger.");
            CheckAndStoreProcesses();
            Console.ReadKey();
        }

        private static void TimerTickEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("1min has passed.");
            CheckAndStoreProcesses();
        }

        private static void CheckAndStoreProcesses()
        {
            Process[] allProcesses = Process.GetProcesses(); // Get all processes.
            List<Process> allProcessesNoDup = allProcesses.GroupBy(p => p.ProcessName).Select(g => g.First()).ToList(); // Remove duplicate process names.

            SQLiteConnection sqliteConnection = new SQLiteConnection("Data Source=ProgramTrak.sqlite;Version=3;");
            sqliteConnection.Open();

            foreach (Process process in allProcessesNoDup)
            {
                Console.WriteLine(process.ProcessName);

                // Read from DB to see if there is already an entry.
                SQLiteCommand sqliteCommand = new SQLiteCommand("SELECT * FROM TestTable where ProcessName ='" + process.ProcessName + "'", sqliteConnection);
                SQLiteDataReader sqliteDataReader = sqliteCommand.ExecuteReader();

                if (sqliteDataReader.Read())
                {
                    if (process.ProcessName == sqliteDataReader["ProcessName"].ToString())
                    {
                        sqliteCommand = new SQLiteCommand("Update TestTable SET ActiveMins =" + (int.Parse(sqliteDataReader["ActiveMins"].ToString()) + 1) + " WHERE ProcessName = '" + process.ProcessName + "'", sqliteConnection);
                        sqliteCommand.ExecuteNonQuery();
                    }
                    else
                    {
                        sqliteCommand = new SQLiteCommand("INSERT INTO TestTable(ProcessName, ActiveMins) VALUES ('" + process.ProcessName + "', " + 1 + ")", sqliteConnection);
                        sqliteCommand.ExecuteNonQuery();
                    }
                }
                else
                {
                    sqliteCommand = new SQLiteCommand("INSERT INTO TestTable(ProcessName, ActiveMins) VALUES ('" + process.ProcessName + "', " + 1 + ")", sqliteConnection);
                    sqliteCommand.ExecuteNonQuery();
                }

                sqliteDataReader.Close();
            }

            sqliteConnection.Close();
        }
    }
}
